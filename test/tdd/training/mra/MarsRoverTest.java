package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MarsRoverTest {
	private List<String> planetObstacles;
	
	@Before
	public void initializePlanetObstaclesList() {
		planetObstacles = new ArrayList<>();
	}
	
	// User story 1
	@Test
	public void testPlanetContainsObstacleAt() throws MarsRoverException {
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover mRover = new MarsRover(10, 10, planetObstacles);
		assertTrue(mRover.planetContainsObstacleAt(4, 7));
		assertTrue(mRover.planetContainsObstacleAt(2, 3));
		assertFalse(mRover.planetContainsObstacleAt(3, 3));
	}
	
	// User story 2, 3
	@Test
	public void testExecuteCommand() throws MarsRoverException {
		MarsRover mRover = new MarsRover(10, 10, planetObstacles);
		assertEquals(mRover.executeCommand(""), "(0,0,N)");
		assertEquals(mRover.executeCommand("r"), "(0,0,E)");
		mRover.executeCommand("l");
		assertEquals(mRover.executeCommand("l"), "(0,0,W)");
	}
	
	// User story 4
	@Test
	public void testExecuteCommandMoveForward() throws MarsRoverException {
		MarsRover mRover = new MarsRover(10, 10, planetObstacles);
		assertEquals(mRover.executeCommand("f"), "(0,1,N)");
		assertEquals(mRover.executeCommand("f"), "(0,2,N)");
		mRover.executeCommand("r");
		assertEquals(mRover.executeCommand("f"), "(1,2,E)");
	}
	
	// User story 5
	@Test
	public void testExecuteCommandMoveBackwards() throws MarsRoverException {
		MarsRover mRover = new MarsRover(10, 10, planetObstacles);
		mRover.executeCommand("f");
		assertEquals(mRover.executeCommand("b"), "(0,0,N)");
	}
	
	// User story 6
	@Test
	public void testExecuteCommandCombination() throws MarsRoverException {
		MarsRover mRover = new MarsRover(10, 10, planetObstacles);
		assertEquals(mRover.executeCommand("rrl"), "(0,0,E)");
		mRover.executeCommand("l");
		assertEquals(mRover.executeCommand("fb"), "(0,0,N)");
	}
	
	// User story 7
	@Test
	public void testExecuteCommandWrapping() throws MarsRoverException {
		MarsRover mRover = new MarsRover(10, 10, planetObstacles);
		assertEquals(mRover.executeCommand("b"), "(0,9,N)");
	}
	
	// User story 8
	@Test
	public void testExecuteCommandSingleObstacle() throws MarsRoverException {
		planetObstacles.add("(2,2)");
		MarsRover mRover = new MarsRover(10, 10, planetObstacles);
		assertEquals(mRover.executeCommand("ffrfff"), "(1,2,E)(2,2)");
	}
	
	// User story 9
	@Test
	public void testExecuteCommandMultipleObstacle() throws MarsRoverException {
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		MarsRover mRover = new MarsRover(10, 10, planetObstacles);
		assertEquals(mRover.executeCommand("ffrfffrflf"), "(1,1,E)(2,2)(2,1)");
	}
	
	// User story 10
	@Test
	public void testExecuteCommandWrappingAndObstacle() throws MarsRoverException {
		planetObstacles.add("(0,9)");
		MarsRover mRover = new MarsRover(10, 10, planetObstacles);
		assertEquals(mRover.executeCommand("b"), "(0,0,N)(0,9)");
	}
	
	// User story 11
	@Test
	public void testExecuteCommandTour() throws MarsRoverException {
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		MarsRover mRover = new MarsRover(6, 6, planetObstacles);
		assertEquals(mRover.executeCommand("ffrfffrbbblllfrfrbbl"), "(0,0,N)(2,2)(0,5)(5,0)");
	}
}
