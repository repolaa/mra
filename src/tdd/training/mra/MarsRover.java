package tdd.training.mra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MarsRover {
	int planetX;
	int planetY;
	boolean[][] planet;
	List<String> directions;
	int roverX;
	int roverY;
	int roverDirection;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planet = new boolean [planetX][planetY];
		this.directions = Arrays.asList("N", "E", "S", "W");
		this.roverX = 0;
		this.roverY = 0;
		this.roverDirection = 0; // 0 -> North
		if(planetX < 0 || planetY < 0)
			throw new MarsRoverException("The coordinates cannot be negative");
		this.planetX = planetX;
		this.planetY = planetY;
		for(int i=0; i<planetX; i++)
			for(int j=0; j<planetY; j++)
				planet[i][j] = false;
		
		int xCoordinate = 0;
		int yCoordinate = 0;
		for(String s : planetObstacles) {
			try {
				xCoordinate = Integer.parseInt(s.substring(s.indexOf("(")+1, s.indexOf(",")));
				yCoordinate = Integer.parseInt(s.substring(s.indexOf(",")+1, s.indexOf(")")));
				planet[xCoordinate][yCoordinate] = true;
			} catch (Exception e) {
				throw new MarsRoverException("Invalid list of planet obstacles");
			}
		}		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if(x>=planetX || y>=planetY || x<0 || y<0)
			throw new MarsRoverException("Invalid cell (outside the borders)");
		return planet[x][y];
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		char[] commands = commandString.toCharArray();
		List<String> encounteredObstacles = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		
		if(commandString.equals(""))
			return "("+roverX+","+roverY+","+directions.get(roverDirection)+")";
		
		for(char command : commands) {
			if(command == 'r')
				roverDirection = (roverDirection+1)%4;
			else if(command == 'l') {
				if(roverDirection == 0)
					roverDirection = directions.size()-1;
				else
					roverDirection--;
			}
			else if(command == 'f')
				move(encounteredObstacles, 1);
			else if(command == 'b')
				move(encounteredObstacles, -1);
			else throw new MarsRoverException("Invalid command");	
		}
		
		for(String s : encounteredObstacles)
			sb.append(s);
		return "("+roverX+","+roverY+","+directions.get(roverDirection)+")"+sb;
	}

	private void move(List<String> encounteredObstacles, int dir) {
		if(roverDirection==0) {
			if(isObstacle(roverX, (roverY+dir)%planetY, encounteredObstacles))
				return;
			roverY = (roverY+dir)%planetY;
			if(roverY<0)
				roverY = planetY - 1;
		}
		if(roverDirection==1) {
			if(isObstacle((roverX+dir)%planetX, roverY, encounteredObstacles))
				return;
			roverX = (roverX+dir)%planetX;
			if(roverX<0)
				roverX = planetX - 1;
		}
		if(roverDirection==2) {
			if(isObstacle(roverX, (roverY-dir)%planetY, encounteredObstacles))
				return;
			roverY = (roverY-dir)%planetY;
			if(roverY<0)
				roverY = planetY - 1;
		}
		if(roverDirection==3) {
			if(isObstacle((roverX-dir)%planetX, roverY, encounteredObstacles))
				return;
			roverX = (roverX-dir)%planetX;
			if(roverX<0)
				roverX = planetX - 1;
		}
	}
	
	private boolean isObstacle(int xCoordinate, int yCoordinate, List<String> encounteredObstacles) {
		if(xCoordinate<0)
			xCoordinate = planetX - 1;
		if(yCoordinate<0)
			yCoordinate = planetY - 1;
		if(planet[xCoordinate][yCoordinate]) {
			String obstacle = String.format("(%d,%d)", xCoordinate, yCoordinate);
			if(encounteredObstacles.contains(obstacle))
				return true;
			encounteredObstacles.add(obstacle);
			return true;
		}
		return false;
	}

}
